const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const feedBackController = require("../Controllers/feedBack");

/* POST */
router.post(
	"/save",
	body("satisfaction")
		.isInt({ min: 0, max: 5 })
		.withMessage(
			"Satisfaction level can not be less than zero or greater than five"
		),
	body("customerService")
		.isInt({ min: 0, max: 5 })
		.withMessage(
			"Satisfaction level can not be less than zero or greater than five"
		),
	body("payment")
		.notEmpty()
		.isInt({ min: 0, max: 5 })
		.withMessage(
			"Satisfaction level can not be less than zero or greater than five"
		),
	body("comments").isString().withMessage("Comments is not a string"),
	feedBackController.saveFeedBack
);

module.exports = router;
