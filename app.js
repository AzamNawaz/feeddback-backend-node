const express = require("express");
const mongoose = require("mongoose");
const app = express();
const feedbackRoutes = require("./Routes/feedBack");
const port = process.env.PORT || 8080
app.use((req, res, next) => {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader(
		"Access-Control-Allow-Methods",
		"OPTIONS , PUT ,POST,PATCH,DELETE,GET"
	);
	res.setHeader("Access-Control-Allow-Headers", "Content-Type , Authorization");
	next();
});
/* Parsers */
app.use(express.json());
/* Routes */
app.use("/feedback",feedbackRoutes);

/* Errors */
app.use((error, req, res, next) => {
	error.status = error.status ? error.status : 500;
	res.json({
		info: {
			status: error.status,
			message: error.message,
		},
		error: error,
	});
});
mongoose
	.connect(
		"mongodb+srv://Azam:azmnwz0074993434@cluster0.ufqei.mongodb.net/SolavieveFeedBack?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
	)
	.then((client) => {
		app.listen(port);
	})
	.catch((err) => {
		console.log(err);
	});
