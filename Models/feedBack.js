const mongoDb = require("mongoose");
const Schema = mongoDb.Schema;

const Feedback = new Schema(
	{
		satisfaction: {
			type: Number,
			min: [0, "Value can not be less than zero"],
			max: [5, "Value can not be greater then 5"],
			required: true,
			default: 0,
		},
		customerService: {
			type: Number,
			min: [0, "Value can not be less than zero percent"],
			max: [5, "Value can not be greater then 5"],
			required: true,
			default: 0,
		},
		payment: {
			type: Number,
			min: [0, "Value can not be less than zero"],
			max: [5, "Value can not be greater then 5"],
			required: true,
			default: 0,
		},
		comments: {
			type: String,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoDb.model("feedback", Feedback);
