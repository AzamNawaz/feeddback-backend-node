const FeedBack = require("../Models/feedBack");
const { validationResult } = require("express-validator");
exports.saveFeedBack = (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		console.log(errors);
		console.log(errors.errors[0].msg);
		res
			.status(400)
			.send(errors.errors[0].msg + "-- param =" + errors.errors[0].param);
		return;
	}

	const data = req.body;
	const feedback = new FeedBack({
		satisfaction: data.satisfaction,
		customerService: data.customerService,
		payment: data.payment,
		comments: data.comments,
	});
	feedback
		.save()
		.then((result) => {
			if (!result) {
				res.staus(400).json({
					info: {
						status: 400,
						message: "Someting went wrong could not save data",
					},
				});
				return;
			}
			res.json({
				info: {
					status: 200,
					message: "Feedback saved",
				},
			});
		})
		.catch((error) => {
			next(error);
		});
};
